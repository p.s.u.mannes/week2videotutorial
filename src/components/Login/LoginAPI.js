export const LoginAPI = {
    register(registerDetails) {
        //probably better to have this repeating part in a utils file
        const requestOptions = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: registerDetails
        }

        return fetch("https://noroff-movie-catalogue.herokuapp.com/v1/users/register", requestOptions)
            .then(response => response.json()) //returns a promise
            .then(data => data.data)
    },
    login(loginDetails) {
        const requestOptions = {
            method: "POST",
            headers: { "Content-Type": "application/json"},
            body: loginDetails
        }

        return fetch("https://noroff-movie-catalogue.herokuapp.com/v1/users/login", requestOptions)
            .then(response => response.json()) //returns a promise
            .then(data => data.data)
    }
}
// importing dependancies...

//import Vue from 'vue' dependancy
import Vue from 'vue'
//import vuex terminal
import Vuex from 'vuex'
import  { LoginAPI } from "@/components/Login/LoginAPI"
import { MoviesAPI } from "@/components/Movie/MoviesAPI"

Vue.use(Vuex)

export default new Vuex.Store({
    //define application state in js object:
    state: {
        username: '',
        password: '',
        profile: {},
        movies: [],
        searchText: '',
        error: '',
    },

    //define mutations (things that change)
    //mutators change the state
    mutations: {
        //function takes state of application
        //payload = desired state, what you want to set state to
        setProfile: (state, payload) => {
            state.profile = payload
        },
        setMovies: (state, payload) => {
            state.movies = payload
        },
        addFavourite: (state, payload) => {
            state.profile.favourites = payload
        },
        setUsername: (state, payload) => {
            state.profile.favourites = payload
        },
        setPassword: (state, payload) => {
            state.password = payload
        },
        setSearchText: (state, payload) => {
            state.searchText = payload
        },
        setError: (state, payload) => {
            state.error = payload
        }
    },

    //filtering done in getters...g
    //e.g. getting the value of a state after doing operations on it
    getters: {

    },

    // all async methods go here
    actions: {
        async loginUser({ state, commit }) {
            try {
                const loginDetails = JSON.stringify({
                    user: {
                        username: state.username,
                        password: state.password
                    }
                })

                const user = await LoginAPI.login(loginDetails)

                if (user) {
                    commit('setProfile', user)
                } else {
                    commit('setError', 'User was not found')
                }

            } catch (e) {
                commit('setError', e.message)
            }
        },

        async registerUser({ state, commit }) {
            try {
                const registerDetails = JSON.stringify({
                    user: {
                        username: state.username,
                        password: state.password
                    }
                })

                const user = await LoginAPI.register(registerDetails)
                if (user) {
                    commit('setProfile', user)
                } else {
                    commit('setError', 'User could not be registered')
                }
            } catch (e) {
                commit('setError', e.message)
            }
        },
        // object destructuring with { } to be able to access commit, state directly
        async fetchMovies({ commit }) {
            try {
                const movies = await MoviesAPI.fetchMovies()
                commit('setMovies', movies)
            } catch (e) {
                commit('setError', e.message)
            }
        },

        // The keyword async before a function makes the function return a promise:
        // await makes a function wait for a Promise
        // The promise is a placeholder holding the result of an asynchronous operation. 
        // If the operation completes successfully, then the promise fulfills with the operation value, 
        // but if the operation fails: the promise rejects with the reason of the failure. 
        // Promises can also create chains, which are useful in handling multiple dependent async operations.
        async addFavourite({ commit, state }, payload) {
            const { profile } = state
            
            try {
                const favourite = {
                    //payload is the interesting part of data in an API
                    movieId: payload,
                    userId: profile.id
                }

                const newFavourites = await MoviesAPI.addFavourite(favourite)
                commit('addFavourite', newFavourites)
            } catch (e) {

                //“commit” with parameter true commits the changes and updates the UI
                //we have vue to update the UI in our single-page application
                //no need for links to go on different webpages, which makes
                //our webpage faster
                commit('setError', e.message)
            }
            

            
        }
    }
})